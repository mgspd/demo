package com.monster.demo.applicant.controller;

import com.monster.demo.applicant.dto.ApplicantDTO;
import com.monster.demo.applicant.service.ApplicantService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/*
Types that carry this annotation are treated as controllers where @RequestMapping
methods assume @ResponseBody semantics by default.

RequestMapping - annotation for mapping web requests onto classes/methods
ResponseBody - annotation that indicates a method return value should be bound to the web response body.
 */
@RestController
@RequestMapping("/api/applicants")
public class ApplicantController {

  private final ApplicantService applicantService;

  @Autowired
  public ApplicantController(ApplicantService applicantService) {
    this.applicantService = applicantService;
  }

  // Swagger - describes an operation or typically a HTTP method against a specific path.
  @ApiOperation(value = "Returns applicant", response = ApplicantDTO.class)
  // PathVariable - annotation which indicates that a method parameter should be bound to a URI template variable
  @GetMapping("/{applicantId}") // Annotation for mapping HTTP GET requests onto specific handler methods.
  public ApplicantDTO getApplicant(@PathVariable("applicantId") Integer applicantId) {
    return applicantService.getApplicant(applicantId);
  }

  @ApiOperation(value = "Creates new applicant")
  @PostMapping
  public void createApplicant(ApplicantDTO applicant) {
    applicantService.createApplicant(applicant);
  }
}
