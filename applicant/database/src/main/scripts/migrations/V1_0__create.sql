use applicant;

-- set default character set and collation
ALTER DATABASE applicant CHARACTER SET utf8 COLLATE utf8_general_ci;

-- create users and grants
grant select, update, insert, delete on applicant.* TO applicant identified by 'applicant1234';

-- create all the other stuff
CREATE TABLE APPLICANT
(
   ID    INT NOT NULL AUTO_INCREMENT,
   FIRST_NAME           VARCHAR(50),
   LAST_NAME            VARCHAR(50),
   EMAIL                VARCHAR(50),
   NATIONAL_ID          VARCHAR(20),
   CITIZENSHIP          VARCHAR(50),
   ADDRESS1              VARCHAR(200),
   ADDRESS2             VARCHAR(200),
   CITY                 VARCHAR(50),
   STATE                VARCHAR(50),
   COUNTRY              VARCHAR(50),
   POSTAL_CODE          VARCHAR(50),
   PRIMARY KEY (ID)
)
ENGINE = INNODB;



