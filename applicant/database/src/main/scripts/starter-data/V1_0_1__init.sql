use applicant;

INSERT INTO APPLICANT(ID, FIRST_NAME, LAST_NAME, EMAIL, NATIONAL_ID, CITIZENSHIP, ADDRESS1, ADDRESS2, CITY, STATE, COUNTRY, POSTAL_CODE)
VALUES (1, 'Jon', 'Snow', 'jon.snow@demo.com', 'JS10001', 'US', 'Winterfell 1', null, 'Winterfell', 'North', 'Westeros','10000');