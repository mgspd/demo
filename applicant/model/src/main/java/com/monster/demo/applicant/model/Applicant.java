package com.monster.demo.applicant.model;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

// Specifies that the class is an entity.
@Entity
public class Applicant {

  // Specifies the primary key of an entity
  @Id
  /*
  Is used to specify the mapped column for a persistent property or field.
  If no Column annotation is specified, the default values apply.
  */
  @Column(name = "ID", nullable = false, updatable = false)
  @GeneratedValue(strategy= GenerationType.IDENTITY)
  private Integer id;

  // Won’t allow null values for the constrained field.
  @NotNull
  private String firstName;

  @NotNull
  private String lastName;

  @NotNull
  private String email;

  private String nationalId;

  private String citizenship;

  @Embedded
  private Address address;

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getNationalId() {
    return nationalId;
  }

  public void setNationalId(String nationalId) {
    this.nationalId = nationalId;
  }

  public String getCitizenship() {
    return citizenship;
  }

  public void setCitizenship(String citizenship) {
    this.citizenship = citizenship;
  }

  public Address getAddress() {
    return address;
  }

  public void setAddress(Address address) {
    this.address = address;
  }
}
