package com.monster.demo.applicant.persistence;

import com.monster.demo.applicant.model.Applicant;
import org.springframework.data.jpa.repository.JpaRepository;

/*
JpaRepository provides some JPA-related methods such as flushing the persistence context and deleting records in a batch.
 */
public interface ApplicantRepository extends JpaRepository<Applicant, Integer> {
}
