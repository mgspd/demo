package com.monster.demo.applicant.service;

import com.monster.demo.applicant.dto.ApplicantDTO;
import com.monster.demo.applicant.model.Applicant;
import org.mapstruct.Mapper;

/*
Marks an interface or abstract class as a mapper and activates the
generation of a implementation of that type via MapStruct.
 */
@Mapper(componentModel = "spring") // the generated mapper is a Spring bean and can be retrieved via @Autowired
public interface ApplicantMapper {

  ApplicantDTO map(Applicant applicant);

  Applicant map(ApplicantDTO applicant);
}