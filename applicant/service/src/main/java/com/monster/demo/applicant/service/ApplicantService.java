package com.monster.demo.applicant.service;

import com.monster.demo.applicant.dto.ApplicantDTO;

public interface ApplicantService {

  ApplicantDTO getApplicant(Integer applicantId);

  void createApplicant(ApplicantDTO applicant);
}
