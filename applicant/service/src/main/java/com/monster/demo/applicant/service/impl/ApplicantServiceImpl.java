package com.monster.demo.applicant.service.impl;

import com.monster.demo.applicant.dto.ApplicantCreatedEventDTO;
import com.monster.demo.applicant.dto.ApplicantDTO;
import com.monster.demo.applicant.model.Applicant;
import com.monster.demo.applicant.persistence.ApplicantRepository;
import com.monster.demo.applicant.service.ApplicantMapper;
import com.monster.demo.applicant.service.ApplicantService;
import com.monster.demo.common.microservice.common.exception.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.messaging.Source;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;

/*
This annotation serves as a specialization of @Component, allowing for implementation classes
to be autodetected through classpath scanning (component holding business logic)
 */
@Service
@EnableBinding(Source.class)
public class ApplicantServiceImpl implements ApplicantService {

  private final ApplicantRepository applicantRepository;
  private final ApplicantMapper applicantMapper;
  private final Source source;

  @Autowired
  public ApplicantServiceImpl(ApplicantRepository applicantRepository, ApplicantMapper applicantMapper, Source source) {
    this.applicantRepository = applicantRepository;
    this.applicantMapper = applicantMapper;
    this.source = source;
  }

  public ApplicantDTO getApplicant(Integer applicantId) {
    Applicant applicant = applicantRepository.findById(applicantId).orElseThrow(
        () -> new NotFoundException("Unable to find applicant for applicantId: " + applicantId));
    return applicantMapper.map(applicant);
  }

  public void createApplicant(ApplicantDTO applicant) {
    // As long as an entity ID is null, record will be created in database, otherwise it will be updated
    applicantRepository.save(applicantMapper.map(applicant));

    publishApplicantCreatedEvent(new ApplicantCreatedEventDTO(
        applicant.getFirstName(),
        applicant.getLastName(),
        applicant.getEmail()));
  }

  private void publishApplicantCreatedEvent(ApplicantCreatedEventDTO applicantCreatedEvent) {
    source.output().send(MessageBuilder.withPayload(applicantCreatedEvent).build());
  }
}
