package com.monster.demo.applicant.service.impl;

import com.monster.demo.applicant.persistence.ApplicantRepository;
import com.monster.demo.common.microservice.common.exception.NotFoundException;
import java.util.Optional;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.cloud.stream.messaging.Source;

@RunWith(MockitoJUnitRunner.class)
public class ApplicantServiceImplUnitTest {

  @Mock
  private ApplicantRepository applicantRepository;

  @Mock
  private Source source;

  @InjectMocks
  private ApplicantServiceImpl applicantService;

  @Test(expected = NotFoundException.class)
  public void testGetApplicantNotFound() {
    Integer testApplicantId = 1;
    Mockito.when(applicantRepository.findById(testApplicantId)).thenReturn(Optional.empty());
    applicantService.getApplicant(testApplicantId);
  }
}
