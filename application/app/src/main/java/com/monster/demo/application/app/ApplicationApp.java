package com.monster.demo.application.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/*
This annotation consists of following important annotations:
SpringBootConfiguration - Indicates that a class provides Spring Boot application @Configuration.
Can be used as an alternative to the Spring's standard @Configuration annotation so that
configuration can be found automatically (for example in tests).

EnableAutoConfiguration - annotation tells Spring Boot to "guess" how you will want to configure Spring,
based on the jar dependencies that you have added. For example, If HSQLDB is on your classpath, and you
have not manually configured any database connection beans, then Spring will auto-configure an in-memory database.

ComponentScan - tells Spring to look for other components, configurations, and services in the specified package.
Spring is able to auto scan, detect and register your beans or components from pre-defined project package.
If no package is specified current class package is taken as the root package. As part of the SpringBootApplication annotation,
components are scanned on the package level of class having this annotation resides.
 */
@SpringBootApplication(scanBasePackages = "com.monster.demo")
// Configures the base packages used by auto-configuration when scanning for entity classes.
@EntityScan("com.monster.demo.application.model")
// Will scan the package of the annotated configuration class for Spring Data repositories by default.
@EnableJpaRepositories(basePackages = "com.monster.demo.application.persistence")
public class ApplicationApp {
	public static void main(String[] args) {
		SpringApplication.run(ApplicationApp.class, args);
	}
}
