package com.monster.demo.application.controller;

import com.monster.demo.application.dto.JobDTO;
import com.monster.demo.application.service.ApplicantService;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/*
Types that carry this annotation are treated as controllers where @RequestMapping
methods assume @ResponseBody semantics by default.

RequestMapping - annotation for mapping web requests onto classes/methods
ResponseBody - annotation that indicates a method return value should be bound to the web response body.
 */
@RestController
@RequestMapping("/api/applicants")
public class ApplicantController {

  private final ApplicantService applicantService;

  @Autowired
  public ApplicantController(ApplicantService applicantService) {
    this.applicantService = applicantService;
  }

  // Swagger - describes an operation or typically a HTTP method against a specific path.
  @ApiOperation(value = "Returns jobs for applicant", response = List.class)
  /*
  Annotation for mapping HTTP GET requests onto specific handler methods.Annotation
  for mapping HTTP GET requests onto specific handler methods.

  PathVariable - annotation which indicates that a method parameter should be bound to a URI template variable
  */
  @GetMapping("/{applicantId}/jobs")
  public List<JobDTO> getJobs(@PathVariable("applicantId") Integer applicantId) {
    return applicantService.getJobs(applicantId);
  }
}
