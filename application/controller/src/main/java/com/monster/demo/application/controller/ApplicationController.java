package com.monster.demo.application.controller;

import com.monster.demo.application.dto.ApplicationDTO;
import com.monster.demo.application.service.ApplicationService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/*
Types that carry this annotation are treated as controllers where @RequestMapping
methods assume @ResponseBody semantics by default.

RequestMapping - annotation for mapping web requests onto classes/methods
ResponseBody - annotation that indicates a method return value should be bound to the web response body.
 */
@RestController
@RequestMapping("/api/applications")
public class ApplicationController {

  private final ApplicationService applicationService;

  @Autowired
  public ApplicationController(ApplicationService applicationService) {
    this.applicationService = applicationService;
  }

  // Swagger - describes an operation or typically a HTTP method against a specific path.
  @ApiOperation(value = "Returns application", response = ApplicationDTO.class)
  /*
  Annotation for mapping HTTP GET requests onto specific handler methods.Annotation
  for mapping HTTP GET requests onto specific handler methods.

  PathVariable - annotation which indicates that a method parameter should be bound to a URI template variable
  */
  @GetMapping("/{applicationId}")
  public ApplicationDTO getApplication(@PathVariable("applicationId") Integer applicationId) {
    return applicationService.getApplication(applicationId);
  }
}
