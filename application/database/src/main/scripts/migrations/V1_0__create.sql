use application;

-- set default character set and collation
ALTER DATABASE application CHARACTER SET utf8 COLLATE utf8_general_ci;

-- create users and grants
grant select, update, insert, delete on application.* TO application identified by 'application1234';
grant select, update, insert, delete on application.* TO test identified by 'test1234';

-- create all the other stuff
CREATE TABLE APPLICATION
(
   ID    INT NOT NULL AUTO_INCREMENT,
   APPLICANT_ID INT NOT NULL,
   JOB_ID INT NOT NULL,
   PRIMARY KEY (ID)
)
ENGINE = INNODB;



