package com.monster.demo.application.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/*
We do not want dependency to applicant-dto from applicant microservice, owners of application microservice
should be completely independent of applicant microservice (they should transform data obtained from
applicant microservice into their own required representation of applicant data, that is related to application)
*/
// Provides additional information about Swagger models.
@ApiModel
public class ApplicantDTO {
  @ApiModelProperty("First name of the applicant.")
  private String firstName;
  @ApiModelProperty("Last name of the applicant.")
  private String lastName;
  @ApiModelProperty("Email of the applicant.")
  private String email;
  @ApiModelProperty("National ID of the applicant.")
  private String nationalId;
  @ApiModelProperty("Citizenship of the applicant.")
  private String citizenship;
  @ApiModelProperty("Address of the applicant.")
  private AddressDTO address;

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getNationalId() {
    return nationalId;
  }

  public void setNationalId(String nationalId) {
    this.nationalId = nationalId;
  }

  public String getCitizenship() {
    return citizenship;
  }

  public void setCitizenship(String citizenship) {
    this.citizenship = citizenship;
  }

  public AddressDTO getAddress() {
    return address;
  }

  public void setAddress(AddressDTO address) {
    this.address = address;
  }
}