package com.monster.demo.application.dto;

public class ApplicationDTO {

  private ApplicantDTO applicant;
  private JobDTO job;

  public ApplicantDTO getApplicant() {
    return applicant;
  }

  public void setApplicant(ApplicantDTO applicant) {
    this.applicant = applicant;
  }

  public JobDTO getJob() {
    return job;
  }

  public void setJob(JobDTO job) {
    this.job = job;
  }
}
