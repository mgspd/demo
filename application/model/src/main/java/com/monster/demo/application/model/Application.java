package com.monster.demo.application.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

// Specifies that the class is an entity.
@Entity
public class Application {

  // Specifies the primary key of an entity
  @Id
  /*
  Is used to specify the mapped column for a persistent property or field.
  If no Column annotation is specified, the default values apply.
  */
  @Column(name = "ID", nullable = false, updatable = false)
  private Integer id;

  @NotNull
  private Integer applicantId;

  @NotNull
  private Integer jobId;

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public Integer getApplicantId() {
    return applicantId;
  }

  public void setApplicantId(Integer applicantId) {
    this.applicantId = applicantId;
  }

  public Integer getJobId() {
    return jobId;
  }

  public void setJobId(Integer jobId) {
    this.jobId = jobId;
  }
}