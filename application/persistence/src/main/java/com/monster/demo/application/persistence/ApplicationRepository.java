package com.monster.demo.application.persistence;

import com.monster.demo.application.model.Application;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/*
JpaRepository provides some JPA-related methods such as flushing the persistence context and deleting records in a batch.
 */
public interface ApplicationRepository extends JpaRepository<Application, Integer> {

  List<Application> findAllByApplicantId(Integer applicantId);
}
