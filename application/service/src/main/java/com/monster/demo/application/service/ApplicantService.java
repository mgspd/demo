package com.monster.demo.application.service;

import com.monster.demo.application.dto.JobDTO;
import java.util.List;

public interface ApplicantService {

  List<JobDTO> getJobs(Integer applicantId);

}
