package com.monster.demo.application.service;

import com.monster.demo.application.dto.ApplicantDTO;
import com.monster.demo.common.microservice.client.RestClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.util.UriComponentsBuilder;

@Component
public class ApplicantServiceClient {

  private final RestClient restClient;

  @Value("${microservice.applicant.url}")
  private String applicantServiceUrl;

  /*
  Marks a constructor, field, setter method or config method as to be autowired by Spring's dependency injection facilities
   */
  @Autowired
  public ApplicantServiceClient(RestClient restClient) {
    this.restClient = restClient;
  }

  public ApplicantDTO getApplicant(Integer applicantId) {
    if (applicantId == null) {
      return null;
    }
    return restClient.get(getApplicantUri(applicantId), ApplicantDTO.class);
  }

  private String getApplicantUri(Integer applicantId) {
    return UriComponentsBuilder.fromHttpUrl(applicantServiceUrl)
        .pathSegment(applicantId.toString())
        .toUriString();
  }
}
