package com.monster.demo.application.service;

import com.monster.demo.application.dto.ApplicationDTO;
import com.monster.demo.application.model.Application;
import org.mapstruct.Mapper;

/*
Marks an interface or abstract class as a mapper and activates the
generation of a implementation of that type via MapStruct.
 */
@Mapper(componentModel = "spring") // the generated mapper is a Spring bean and can be retrieved via @Autowired
public interface ApplicationMapper {

  ApplicationDTO map(Application applicant);
}