package com.monster.demo.application.service;

import com.monster.demo.application.dto.ApplicationDTO;


public interface ApplicationService {

  ApplicationDTO getApplication(Integer applicationId);
}
