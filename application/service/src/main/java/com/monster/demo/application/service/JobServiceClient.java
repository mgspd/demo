package com.monster.demo.application.service;

import com.monster.demo.application.dto.JobDTO;
import com.monster.demo.common.microservice.client.RestClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.util.UriComponentsBuilder;

@Component
public class JobServiceClient {

  private final RestClient restClient;

  @Value("${microservice.job.url}")
  private String jobServiceUrl;

  @Autowired
  public JobServiceClient(RestClient restClient) {
    this.restClient = restClient;
  }

  public JobDTO getJob(Integer jobId) {
    if (jobId == null) {
      return null;
    }
    return restClient.get(getJobUri(jobId), JobDTO.class);
  }

  private String getJobUri(Integer jobId) {
    return UriComponentsBuilder.fromHttpUrl(jobServiceUrl)
            .pathSegment(jobId.toString())
            .toUriString();
  }
}
