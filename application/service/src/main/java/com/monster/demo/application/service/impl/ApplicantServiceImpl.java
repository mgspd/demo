package com.monster.demo.application.service.impl;

import com.monster.demo.application.dto.JobDTO;
import com.monster.demo.application.model.Application;
import com.monster.demo.application.persistence.ApplicationRepository;
import com.monster.demo.application.service.ApplicantService;
import com.monster.demo.application.service.JobServiceClient;
import com.monster.demo.common.microservice.common.exception.NotFoundException;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

@Service
public class ApplicantServiceImpl implements ApplicantService {

  private final JobServiceClient jobServiceClient;
  private final ApplicationRepository applicationRepository;

  public ApplicantServiceImpl(JobServiceClient jobServiceClient, ApplicationRepository applicationRepository) {
    this.jobServiceClient = jobServiceClient;
    this.applicationRepository = applicationRepository;
  }

  public List<JobDTO> getJobs(Integer applicantId) {
    List<Application> applications = applicationRepository.findAllByApplicantId(applicantId);
    if (CollectionUtils.isEmpty(applications)) {
      throw new NotFoundException("Unable to find any application for applicantId: " + applicantId);
    }

    return applications.stream()
        .map(a -> jobServiceClient.getJob(a.getJobId()))
        .collect(Collectors.toList());
  }
}
