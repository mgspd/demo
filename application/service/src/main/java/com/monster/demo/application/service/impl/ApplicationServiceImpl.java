package com.monster.demo.application.service.impl;

import com.monster.demo.application.dto.ApplicationDTO;
import com.monster.demo.application.model.Application;
import com.monster.demo.application.persistence.ApplicationRepository;
import com.monster.demo.application.service.ApplicantServiceClient;
import com.monster.demo.application.service.ApplicationMapper;
import com.monster.demo.application.service.ApplicationService;
import com.monster.demo.application.service.JobServiceClient;
import com.monster.demo.common.microservice.common.exception.NotFoundException;
import org.springframework.stereotype.Service;

@Service
public class ApplicationServiceImpl implements ApplicationService {

  private final ApplicantServiceClient applicantServiceClient;
  private final JobServiceClient jobServiceClient;
  private final ApplicationRepository applicationRepository;
  private final ApplicationMapper applicationMapper;

  public ApplicationServiceImpl(
      ApplicantServiceClient applicantServiceClient,
      JobServiceClient jobServiceClient,
      ApplicationRepository applicationRepository,
      ApplicationMapper applicationMapper) {
    this.applicantServiceClient = applicantServiceClient;
    this.jobServiceClient = jobServiceClient;
    this.applicationRepository = applicationRepository;
    this.applicationMapper = applicationMapper;
  }

  public ApplicationDTO getApplication(Integer applicationId) {
    Application applicationModel = applicationRepository.findById(applicationId).orElseThrow(
        () -> new NotFoundException("Unable to find application for applicationId: " + applicationId));

    ApplicationDTO application = applicationMapper.map(applicationModel);
    application.setApplicant(applicantServiceClient.getApplicant(applicationModel.getApplicantId()));
    application.setJob(jobServiceClient.getJob(applicationModel.getJobId()));
    return application;
  }
}
