package com.monster.demo.application.service.impl;

import com.monster.demo.application.dto.ApplicantDTO;
import com.monster.demo.application.dto.ApplicationDTO;
import com.monster.demo.application.dto.JobDTO;
import com.monster.demo.application.service.ApplicantServiceClient;
import com.monster.demo.application.service.ApplicationService;
import com.monster.demo.application.service.JobServiceClient;
import com.monster.demo.common.test.HibernateTestConfiguration;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.Assert;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
@ContextConfiguration(classes = {HibernateTestConfiguration.class})
public class ApplicationServiceImplIntegrationTest {

  @Autowired
  private ApplicationService applicationService;

  @MockBean
  private ApplicantServiceClient applicantServiceClient;

  @MockBean
  private JobServiceClient jobServiceClient;

  @Test
  public void testGetApplication() {
    Integer testApplicantId = 1;
    Integer testJobId = 1;
    Mockito.when(applicantServiceClient.getApplicant(testApplicantId)).thenReturn(new ApplicantDTO());
    Mockito.when(jobServiceClient.getJob(testJobId)).thenReturn(new JobDTO());

    ApplicationDTO application = applicationService.getApplication(testApplicantId);
    Assert.notNull(application, "Application is null");
  }
}
