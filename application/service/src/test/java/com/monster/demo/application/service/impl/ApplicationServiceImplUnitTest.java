package com.monster.demo.application.service.impl;

import static org.mockito.Mockito.mock;

import com.monster.demo.application.dto.ApplicantDTO;
import com.monster.demo.application.dto.ApplicationDTO;
import com.monster.demo.application.dto.JobDTO;
import com.monster.demo.application.model.Application;
import com.monster.demo.application.persistence.ApplicationRepository;
import com.monster.demo.application.service.ApplicantServiceClient;
import com.monster.demo.application.service.ApplicationMapper;
import com.monster.demo.application.service.JobServiceClient;
import java.util.Optional;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mapstruct.factory.Mappers;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class ApplicationServiceImplUnitTest {

  private static final Integer TEST_APPLICATION_ID = 1;
  private static final Integer TEST_APPLICANT_ID = 1;
  private static final Integer TEST_JOB_ID = 1;
  private static final String TEST_APPLICANT_FIRST_NAME = "John";
  private static final String TEST_APPLICANT_LAST_NAME = "Campbell";
  private static final String TEST_APPLICANT_EMAIL = "john.campbell@test.com";
  private static final String TEST_JOB_POSITION_TITLE = "Frontend Developer - JavaScrip";
  private static final String TEST_JOB_POSITION_DESCRIPTION = "Frontend Developer - JavaScript, Angular, JQuery";

  private ApplicantServiceClient applicantServiceClient;
  private JobServiceClient jobServiceClient;
  private ApplicationRepository applicationRepository;
  private ApplicationMapper applicationMapper;
  private ApplicationServiceImpl applicationService;

  @Before
  public void setupTests() {
    this.applicantServiceClient = mock(ApplicantServiceClient.class);
    this.jobServiceClient = mock(JobServiceClient.class);
    this.applicationRepository = mock(ApplicationRepository.class);
    this.applicationMapper = Mappers.getMapper(ApplicationMapper.class);
    this.applicationService = new ApplicationServiceImpl(applicantServiceClient, jobServiceClient, applicationRepository, applicationMapper);
  }

  @Test
  public void testGetApplication() {
    Optional<Application> testModel = createTestApplicationModel();
    ApplicantDTO testApplicant = createTestApplicantDTO();
    JobDTO testJob = createTestJobDTO();

    Mockito.when(applicationRepository.findById(TEST_APPLICATION_ID)).thenReturn(testModel);
    Mockito.when(applicantServiceClient.getApplicant(TEST_APPLICANT_ID)).thenReturn(testApplicant);
    Mockito.when(jobServiceClient.getJob(TEST_JOB_ID)).thenReturn(testJob);

    ApplicationDTO application = applicationService.getApplication(TEST_APPLICATION_ID);
    Assert.assertEquals(testApplicant.getFirstName(), application.getApplicant().getFirstName());
    Assert.assertEquals(testApplicant.getLastName(), application.getApplicant().getLastName());
    Assert.assertEquals(testApplicant.getEmail(), application.getApplicant().getEmail());

    Assert.assertEquals(testJob.getPositionTitle(), application.getJob().getPositionTitle());
    Assert.assertEquals(testJob.getPositionDescription(), application.getJob().getPositionDescription());
  }

  private Optional<Application> createTestApplicationModel() {
    Application application = new Application();
    application.setId(TEST_APPLICATION_ID);
    application.setApplicantId(TEST_APPLICANT_ID);
    application.setJobId(TEST_JOB_ID);
    return Optional.of(application);
  }

  private ApplicantDTO createTestApplicantDTO() {
    ApplicantDTO applicant = new ApplicantDTO();
    applicant.setFirstName(TEST_APPLICANT_FIRST_NAME);
    applicant.setLastName(TEST_APPLICANT_LAST_NAME);
    applicant.setEmail(TEST_APPLICANT_EMAIL);
    return applicant;
  }

  private JobDTO createTestJobDTO() {
    JobDTO job = new JobDTO();
    job.setPositionTitle(TEST_JOB_POSITION_TITLE);
    job.setPositionDescription(TEST_JOB_POSITION_DESCRIPTION);
    return job;
  }
}
