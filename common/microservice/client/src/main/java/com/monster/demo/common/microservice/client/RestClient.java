package com.monster.demo.common.microservice.client;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.monster.demo.common.microservice.common.error.WebServiceErrorResponse;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Collections;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.BufferingClientHttpRequestFactory;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.StreamUtils;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;

@Component
public class RestClient {

  private static final Logger LOG = LoggerFactory.getLogger(RestClient.class);
  private ObjectMapper jsonObjectMapper = new ObjectMapper();


  @Bean
  // Annotation to mark a RestTemplate bean to be configured to use a LoadBalancerClient.
  @LoadBalanced
  RestTemplate restTemplate() {
    if (LOG.isTraceEnabled()) {
      restTemplate = new RestTemplate(new BufferingClientHttpRequestFactory(new SimpleClientHttpRequestFactory()));
      restTemplate.setInterceptors(Collections.singletonList(new RequestLoggingInterceptor()));
      return restTemplate;
    } else {
      return new RestTemplate();
    }
  }

  @Autowired
  RestTemplate restTemplate;

  public <T> T get(String url, Class<T> responseType) {
    return exchangeWithErrorHandling(url, HttpMethod.GET, null, responseType);
  }

  private <T> T exchangeWithErrorHandling(String url, HttpMethod httpMethod, HttpEntity<?> requestEntity, Class<T> responseType) {
    try {
      return restTemplate.exchange(url, httpMethod, requestEntity, responseType).getBody();
    } catch (HttpStatusCodeException notOkStatusEx) {
      //Handle response with HTTP STATUS other than 200
      WebServiceErrorResponse errorResponse;
      try {
        //Try to parse error response body as standard micro service WebServiceErrorResponse
        errorResponse = jsonObjectMapper.readValue(notOkStatusEx.getResponseBodyAsString(), WebServiceErrorResponse.class);
      } catch (Exception parsingException) {
        //Unknown structure of error response - throw the original exception to upper level
        throw notOkStatusEx;
      }
      throw new RestClientException(errorResponse);
      //Generic exception for other kind of error (service not running...) - RestClientException - will be passed to upper levels and caught by generic handler
    }
  }

  public static class RequestLoggingInterceptor implements ClientHttpRequestInterceptor {

    @Override
    public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution) throws IOException {
      LOG.trace("request method: {}, request URI: {}, request headers: {}, request body: {}", request.getMethod(), request.getURI(), request.getHeaders(),
          new String(body, Charset.forName("UTF-8")));

      ClientHttpResponse response = execution.execute(request, body);

      LOG.trace("response status code: {}, response headers: {}, response body: {}", response.getStatusCode(), response.getHeaders(),
          new String(StreamUtils.copyToByteArray(response.getBody()), Charset.forName("UTF-8")));

      return response;
    }
  }
}
