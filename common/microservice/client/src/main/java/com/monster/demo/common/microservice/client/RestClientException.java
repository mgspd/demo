package com.monster.demo.common.microservice.client;

import com.monster.demo.common.microservice.common.error.WebServiceErrorResponse;

public class RestClientException extends RuntimeException {

  private WebServiceErrorResponse webServiceErrorResponse;

  public RestClientException(WebServiceErrorResponse webServiceErrorResponse) {
    super(webServiceErrorResponse.getMessage());
    this.webServiceErrorResponse = webServiceErrorResponse;
  }

  public WebServiceErrorResponse getWebServiceErrorResponse() {
    return webServiceErrorResponse;
  }
}
