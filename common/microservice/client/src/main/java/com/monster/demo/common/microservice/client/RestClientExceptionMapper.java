package com.monster.demo.common.microservice.client;

import com.monster.demo.common.microservice.common.error.CustomServerError;
import com.monster.demo.common.microservice.common.util.ExceptionMapperUtil;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
@Order(Ordered.HIGHEST_PRECEDENCE)
public class RestClientExceptionMapper {

  private static final Log LOG = LogFactory.getLog(RestClientExceptionMapper.class);

  @ExceptionHandler(value = RestClientException.class)
  public ResponseEntity<CustomServerError> handleMgsRestClientException(HttpServletRequest request, RestClientException ex) {
    ResponseEntity.BodyBuilder responseBuilder = ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR);
    String code = ex.getWebServiceErrorResponse().getRandomCode();
    logException(code, ex, request);

    ResponseEntity response = responseBuilder
            .contentType(org.springframework.http.MediaType.APPLICATION_JSON)
            .body(new CustomServerError(code));

    return response;
  }


  private void logException(String code, RestClientException exception, HttpServletRequest request) {
    final String urlMessage = "URL: " + request.getRequestURL().toString();
    final String exceptionLogMessage = ExceptionMapperUtil.createExceptionLogMessage(urlMessage, code, exception);

    LOG.error("Error when calling external REST service. Debug info from external service: ");
    LOG.error(exception.getWebServiceErrorResponse().getDebugMessage());
    LOG.error(exceptionLogMessage);
  }

}
