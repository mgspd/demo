package com.monster.demo.common.microservice.hibernate;

import org.hibernate.boot.model.naming.Identifier;
import org.hibernate.engine.jdbc.env.spi.JdbcEnvironment;
import org.springframework.boot.orm.jpa.hibernate.SpringPhysicalNamingStrategy;

import java.util.Locale;

/*
Spring Boot configures the physical naming strategy with SpringPhysicalNamingStrategy where all dots
are replaced by underscores and camel casing is replaced by underscores and all table names are generated
in lower case. Additionally we want table names to be upper cased.
*/
public class UpperCaseSpringPhysicalNamingStrategy extends SpringPhysicalNamingStrategy {

  @Override
  protected Identifier getIdentifier(String name, boolean quoted, JdbcEnvironment jdbcEnvironment) {
    String upperCaseName = name.toUpperCase(Locale.ROOT);
    return new Identifier(upperCaseName, quoted);
  }
}
