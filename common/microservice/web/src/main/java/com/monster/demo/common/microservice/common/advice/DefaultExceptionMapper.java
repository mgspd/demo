package com.monster.demo.common.microservice.common.advice;

import com.monster.demo.common.microservice.common.error.CustomServerError;
import com.monster.demo.common.microservice.common.util.ExceptionMapperUtil;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class DefaultExceptionMapper {

  private static final Log LOG = LogFactory.getLog(DefaultExceptionMapper.class);

  @ExceptionHandler(value = Throwable.class)
  public ResponseEntity<CustomServerError> handleGenericError(HttpServletRequest request, Throwable ex) {
    ResponseEntity.BodyBuilder responseBuilder = ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR);
    String code = ExceptionMapperUtil.generateRandomCode();
    logException(code, ex, request);

    ResponseEntity response = responseBuilder
            .contentType(MediaType.APPLICATION_JSON)
            .body(new CustomServerError(code));

    return response;
  }


  private void logException(String code, Throwable exception, HttpServletRequest request) {
    final String urlMessage = "URL: " + request.getRequestURL().toString();
    final String exceptionLogMessage = ExceptionMapperUtil.createExceptionLogMessage(urlMessage, code, exception);

    LOG.error(exceptionLogMessage);
  }
}
