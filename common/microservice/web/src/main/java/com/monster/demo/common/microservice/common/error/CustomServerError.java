package com.monster.demo.common.microservice.common.error;

public class CustomServerError {

  private String code;

  public CustomServerError(String code) {
    this.code = code;
  }

  public String getCode() {
    return code;
  }

  void setCode(String code) {
    this.code = code;
  }
}
