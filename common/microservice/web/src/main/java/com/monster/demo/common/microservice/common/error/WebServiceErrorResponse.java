package com.monster.demo.common.microservice.common.error;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.Date;
import org.apache.commons.lang3.RandomStringUtils;

@ApiModel
public class WebServiceErrorResponse {

   @ApiModelProperty("HTTP status - same as the actual HTTP status of the response.")
   private int httpStatus;
   @ApiModelProperty("Exact date / time when the error occurred.")
   private Date dateTime;
   @ApiModelProperty("Randomly generated code - could be used to quickly find more details about the error in application logs.")
   private String randomCode;
   @ApiModelProperty("Short human readable message describing the error.")
   private String message;
   @ApiModelProperty("Extended information describing the error - e.g. description of specific exception on the server, detail about input validation error, etc.")
   private String debugMessage;

   private WebServiceErrorResponse() {
     dateTime = new Date();
     randomCode = RandomStringUtils.randomNumeric(9);
   }

   public WebServiceErrorResponse(int httpStatus) {
       this();
       this.httpStatus = httpStatus;
   }

  public WebServiceErrorResponse(int httpStatus, Throwable ex) {
       this();
       this.httpStatus = httpStatus;
       this.message = "Unexpected error";
       this.debugMessage = getDetailedErrorMessage(ex);
   }

  public WebServiceErrorResponse(int httpStatus, String message, Throwable ex) {
       this();
       this.httpStatus = httpStatus;
       this.message = message;
       this.debugMessage = getDetailedErrorMessage(ex);
   }

  private static String getDetailedErrorMessage(Throwable throwable) {
    String message = throwable.getClass().getName() + ": " + throwable.getMessage();
    if (throwable.getCause() != null) {
      message += " CAUSED BY: ";
      message += throwable.getCause().getClass().getName() + ": " + throwable.getCause().getMessage();
    }
    return message;
  }

  public int getHttpStatus() {
    return httpStatus;
  }

  public Date getDateTime() {
    return dateTime;
  }

  public String getRandomCode() {
    return randomCode;
  }

  public String getMessage() {
    return message;
  }

  public String getDebugMessage() {
    return debugMessage;
  }
}