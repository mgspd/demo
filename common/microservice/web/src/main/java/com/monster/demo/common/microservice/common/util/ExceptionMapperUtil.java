package com.monster.demo.common.microservice.common.util;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;

import static org.apache.commons.lang3.StringUtils.center;

public class ExceptionMapperUtil {

  private ExceptionMapperUtil() {
    // avoid instantiation
  }

  public static String generateRandomCode() {
    return RandomStringUtils.randomNumeric(9);
  }

  public static String createExceptionLogMessage(String message, String code, Throwable exception) {
    return new StringBuilder().append(System.lineSeparator())
        .append(center(' ' + code + ' ', 72, '*'))
        .append(System.lineSeparator())
        .append(message)
        .append(System.lineSeparator())
        .append(System.lineSeparator())
        .append(ExceptionUtils.getStackTrace(exception))
        .append(System.lineSeparator())
        .append(center("", 72, '*'))
        .append(System.lineSeparator())
        .toString();
  }
}
