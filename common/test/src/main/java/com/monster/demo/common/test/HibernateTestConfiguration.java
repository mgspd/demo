package com.monster.demo.common.test;

import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateProperties;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateSettings;
import org.springframework.boot.autoconfigure.orm.jpa.JpaProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;

@Configuration
@EnableJpaRepositories(
    entityManagerFactoryRef = "testManagerFactory",
    basePackages = "com.monster.demo")
@EntityScan("com.monster.demo")
@ComponentScan("com.monster.demo")
@EnableAutoConfiguration
public class HibernateTestConfiguration {

  @Bean(name = "testManagerFactory")
  public LocalContainerEntityManagerFactoryBean entityManagerFactory(
      EntityManagerFactoryBuilder builder,
      DataSource dataSource,
      JpaProperties jpaProperties,
      HibernateProperties hibernateProperties) {
    Map<String, Object> properties = new HashMap<>();
    properties.putAll(hibernateProperties.determineHibernateProperties(jpaProperties.getProperties(), new HibernateSettings()));
    return builder
        .dataSource(dataSource)
        .packages("com.monster.demo")
        .properties(properties)
        .build();
  }
}
