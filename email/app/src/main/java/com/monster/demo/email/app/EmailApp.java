package com.monster.demo.email.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/*
This annotation consists of following important annotations:
SpringBootConfiguration - Indicates that a class provides Spring Boot application @Configuration.
Can be used as an alternative to the Spring's standard @Configuration annotation so that
configuration can be found automatically (for example in tests).

EnableAutoConfiguration - annotation tells Spring Boot to "guess" how you will want to configure Spring,
based on the jar dependencies that you have added. For example, If HSQLDB is on your classpath, and you
have not manually configured any database connection beans, then Spring will auto-configure an in-memory database.

ComponentScan - tells Spring to look for other components, configurations, and services in the specified package.
Spring is able to auto scan, detect and register your beans or components from pre-defined project package.
If no package is specified current class package is taken as the root package. As part of the SpringBootApplication annotation,
components are scanned on the package level of class having this annotation resides.
 */
@SpringBootApplication(scanBasePackages = "com.monster.demo")
public class EmailApp {
	public static void main(String[] args) {
		SpringApplication.run(EmailApp.class, args);
	}
}
