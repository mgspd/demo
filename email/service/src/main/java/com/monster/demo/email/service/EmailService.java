package com.monster.demo.email.service;

import com.monster.demo.email.dto.ApplicantCreatedEventDTO;

public interface EmailService {

  void handleApplicantCreated(ApplicantCreatedEventDTO applicantCreatedEvent);
}
