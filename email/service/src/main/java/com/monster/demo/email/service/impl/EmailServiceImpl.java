package com.monster.demo.email.service.impl;

import com.monster.demo.email.dto.ApplicantCreatedEventDTO;
import com.monster.demo.email.service.EmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.cloud.stream.messaging.Sink;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Service;

/*
This annotation serves as a specialization of @Component, allowing for implementation classes
to be autodetected through classpath scanning (component holding business logic)
 */
@Service
@EnableBinding(Sink.class)
public class EmailServiceImpl implements EmailService {

  private final JavaMailSender mailSender;

  @Autowired
  public EmailServiceImpl(JavaMailSender mailSender) {
    this.mailSender = mailSender;
  }

  @Override
  @StreamListener(Sink.INPUT)
  public void handleApplicantCreated(@Payload ApplicantCreatedEventDTO applicantCreatedEvent) {
    mailSender.send(createApplicantCreatedMessage(applicantCreatedEvent));
  }

  private SimpleMailMessage createApplicantCreatedMessage(ApplicantCreatedEventDTO applicantCreatedEvent) {
    String applicantFullName = applicantCreatedEvent.getFirstName() + " " + applicantCreatedEvent.getLastName();
    SimpleMailMessage mailMessage = new SimpleMailMessage();
    mailMessage.setSubject("New Applicant Profile");
    mailMessage.setText("New applicant profile for applicant " + applicantFullName + " has been successfully created.");
    mailMessage.setFrom("notifications@monsterdemo.com");
    mailMessage.setTo(applicantCreatedEvent.getEmail());
    return mailMessage;
  }
}
