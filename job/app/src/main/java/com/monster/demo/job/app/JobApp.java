package com.monster.demo.job.app;

import org.socialsignin.spring.data.dynamodb.repository.config.EnableDynamoDBRepositories;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;

@SpringBootApplication(scanBasePackages = "com.monster.demo")
// Configures the base packages used by auto-configuration when scanning for entity classes.
@EntityScan("com.monster.demo.job.model")
// Will scan the package of the annotated configuration class for Spring Data repositories by default.
@EnableDynamoDBRepositories(basePackages = "com.monster.demo.job.persistence")
public class JobApp {

  public static void main(String[] args) {
    SpringApplication.run(JobApp.class, args);
  }

}
