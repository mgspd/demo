package com.monster.demo.job.controller;

import com.monster.demo.job.dto.JobDTO;
import com.monster.demo.job.service.JobService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/jobs")
public class JobController {

  private final JobService jobService;

  @Autowired
  public JobController(JobService jobService) {
    this.jobService = jobService;
  }

  @ApiOperation(value = "Returns job", response = JobDTO.class)
  @GetMapping("/{jobId}")
  public JobDTO getJob(@PathVariable("jobId") Integer jobId) {
    return jobService.getJob(jobId);
  }

  @ApiOperation(value = "Creates new job")
  @PostMapping
  public void createApplicant(JobDTO job) {
    jobService.createJob(job);
  }
}
