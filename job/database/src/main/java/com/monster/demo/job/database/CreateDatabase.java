package com.monster.demo.job.database;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.model.CreateTableRequest;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;

import java.io.File;
import java.io.IOException;

public class CreateDatabase {

  private static DynamoDB dynamoDB;
  private static ObjectMapper mapper;

  static {
    String endpoint = System.getProperty("amazon.dynamodb.endpoint");
    String region = System.getProperty("amazon.dynamodb.region");
    String accessKey = System.getProperty("amazon.aws.accesskey");
    String secretKey = System.getProperty("amazon.aws.secretkey");

    AmazonDynamoDBClientBuilder builder = AmazonDynamoDBClientBuilder.standard()
            .withCredentials(new AWSStaticCredentialsProvider(new BasicAWSCredentials(accessKey, secretKey)))
            .withEndpointConfiguration(new AwsClientBuilder.EndpointConfiguration(endpoint, region));
    AmazonDynamoDB client = builder.build();
    dynamoDB = new DynamoDB(client);
    mapper = new ObjectMapper();
  }

  public static void main(String[] args) {
    try {
      String definitionsPath = args[0];
      File definitionFolder = new File(definitionsPath);
      File[] definitionFiles = definitionFolder.listFiles();
      if (definitionFiles != null) {
        for (File file : definitionFiles) {
          createTable(file);
        }
      }

      String dataPath = args[1];
      File dataFolder = new File(dataPath);
      File[] dataFiles = dataFolder.listFiles();
      if (dataFiles != null) {
        for (File file : dataFiles) {
          insertData(file);
        }
      }
    } catch (Exception e) {
      System.err.println("Creating tables failed: " + e);
      e.printStackTrace();
      System.exit(1);
    } finally {
      dynamoDB.shutdown();
    }
  }

  private static void createTable(File json) throws IOException, InterruptedException {
    CreateTableRequest request = mapper.readValue(json, CreateTableRequest.class);

    try {
      Table table = dynamoDB.getTable(request.getTableName());
      table.delete();
      table.waitForDelete();
    } catch (Exception e) {
      System.out.println(String.format("Table: %s does not exist.", request.getTableName()));
    }
    System.out.println(String.format("Creating table %s", request.getTableName()));
    Table table = dynamoDB.createTable(request);
    table.waitForActive();
    System.out.println(String.format("Table %s created", request.getTableName()));
  }

  private static void insertData(File json) throws IOException {
    String tableName = getTableNameFromFile(json);
    Table table = dynamoDB.getTable(tableName);

    System.out.println(String.format("Inserting data into table %s", tableName));
    JsonNode root = mapper.readTree(json);
    if (root instanceof ArrayNode) {
      root.iterator().forEachRemaining(n -> insertItem(n.toString(), table));
    } else {
      insertItem(root.toString(), table);
    }
    System.out.println(String.format("Data inserted into table %s", tableName));
  }

  private static String getTableNameFromFile(File json) {
    String fileName = json.getName();
    int dotPosition = fileName.lastIndexOf(".");
    return dotPosition == -1 ? fileName : fileName.substring(0, dotPosition);
  }

  private static void insertItem(String json, Table table) {
    Item item = Item.fromJSON(json);
    table.putItem(item);
  }
}