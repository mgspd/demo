package com.monster.demo.job.dto;

import io.swagger.annotations.ApiModelProperty;

import java.util.List;

public class JobDTO {

  @ApiModelProperty("Id")
  private Integer id;
  @ApiModelProperty("Position title")
  private String positionTitle;
  @ApiModelProperty("Position description")
  private String positionDescription;
  @ApiModelProperty("Requirements")
  private List<RequirementDTO> requirements;

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getPositionTitle() {
    return positionTitle;
  }

  public void setPositionTitle(String positionTitle) {
    this.positionTitle = positionTitle;
  }

  public String getPositionDescription() {
    return positionDescription;
  }

  public void setPositionDescription(String positionDescription) {
    this.positionDescription = positionDescription;
  }

  public List<RequirementDTO> getRequirements() {
    return requirements;
  }

  public void setRequirements(List<RequirementDTO> requirements) {
    this.requirements = requirements;
  }
}
