package com.monster.demo.job.dto;

import io.swagger.annotations.ApiModelProperty;

public class RequirementDTO {

    @ApiModelProperty("Name")
    private String name;
    @ApiModelProperty("Description")
    private String description;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
