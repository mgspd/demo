package com.monster.demo.job.persistence;

import com.monster.demo.job.model.Job;
import org.socialsignin.spring.data.dynamodb.repository.EnableScan;
import org.springframework.data.repository.CrudRepository;

@EnableScan
public interface JobRepository extends CrudRepository<Job, Integer> {
}
