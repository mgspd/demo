package com.monster.demo.job.service;

import com.monster.demo.job.dto.JobDTO;
import com.monster.demo.job.model.Job;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface JobMapper {

  JobDTO map(Job job);

  Job map(JobDTO job);
}
