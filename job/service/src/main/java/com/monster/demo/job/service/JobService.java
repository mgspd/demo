package com.monster.demo.job.service;

import com.monster.demo.job.dto.JobDTO;

public interface JobService {

  JobDTO getJob(Integer jobId);

  void createJob(JobDTO job);
}
