package com.monster.demo.job.service.impl;

import com.monster.demo.common.microservice.common.exception.NotFoundException;
import com.monster.demo.job.dto.JobDTO;
import com.monster.demo.job.persistence.JobRepository;
import com.monster.demo.job.service.JobMapper;
import com.monster.demo.job.service.JobService;
import org.springframework.stereotype.Service;

@Service
public class JobServiceImpl implements JobService {

  private final JobRepository jobRepository;
  private final JobMapper jobMapper;

  public JobServiceImpl(JobRepository jobRepository, JobMapper jobMapper) {
    this.jobRepository = jobRepository;
    this.jobMapper = jobMapper;
  }

  public JobDTO getJob(Integer jobId) {
    return jobMapper.map(jobRepository.findById(jobId).orElseThrow(
        () -> new NotFoundException("Unable to find job for jobId: " + jobId)));
  }

  @Override
  public void createJob(JobDTO job) {
    jobRepository.save(jobMapper.map(job));
  }
}
