Microservices Demo 
==========

##Prerequisites
1. OpenJDK 12 (https://jdk.java.net/12/)
2. Apache Maven 3.6.1 (https://maven.apache.org/download.cgi)
3. Intellij IDEA 2019 Ultimate (https://www.jetbrains.com/idea/download/#section=windows)
4. Git For Windows (https://git-scm.com/download/win)

###Environment Variables
1. Set JAVA_HOME pointing to openJDK directory
2. Set M2_HOME pointing to maven directory
3. Add path to <JAVA_HOME>/bin to PATH environment variable
4. Add path to <M2_HOME>/bin to PATH environment variable

###Download/Import project to IDEA
1. Run >git clone https://bitbucket.org/mgspd/demo.git
2. Open Intellij IDEA. After short loading you should see "Welcome to Intellij IDEA" dialog 
3. Click on **Import Project**
4. In the opened dialog (Select File or Directory to Import), select the folder where you cloned the project in step 1.
5. In next dialog, keep "Maven" (or select if not selected) and click **Next**
6. In next dialog, do following: 
    1. Check option "Import Maven projects automatically"
    2. VM options for importer - write **-Xmx1g**
    3. JDK for importer - select use JAVA_HOME
    4. Click **Next**      
7. In next dialog (Select maven projects to import), Click **Next**
8. In next dialog (Select project SDK), do following:
    1. Click **+**
    2. Select **JDK**
    3. In opened dialog, select folder containing your installation of OpenJDK 12 and click **OK**
    4. Click **Next**
9. In next dialog, click **Finish**
10. In a small dialog, that .idea folder may be overwritten, click **Yes**
11. IDEA project will open and you'll have to wait, until it finishes downloading maven dependencies and setting up project (In the bottom right corner, there might be text like "...processes running")
12. Because of the step 10, we have to run **git revert**, so that .idea folder gets back all files, that are stored in git repository

##Additional IDEA build/compiler settings
1. In **<demo_project_folder>/.idea** folder, we keep latest **maven_settings.xml** file used for this project (Please do not modify this file)
2. Go to IDEA settings (shortcut is CTRL + ALT + S) and do following: 
    1. Go to **Build/Execution, Deployment -> Build Tools ->Maven**
    2. Change **Maven home directory** to point to your installation of maven (e.g. C:\apache-maven-3.6.1)
    3. Go to **Build/Execution -> Deployment -> Compiler**
    4. Set build process heap size to 2048

##Installing Kitematic
For ease of use/running of 3rd party required tools locally via poweful GUI, we are using Kitematic
1. Download latest from https://github.com/docker/kitematic/releases 

##Installing MYSQL database in Kitematic
1. Click **+NEW**
2. Search for mysql - it is important to install 5.7 version, so look for 5.7 version tag (in AWS environment, used RDS DB is Aurora, that currently supports this version of MySQL)
3. Click **Create** on *official* mysql
4. Go to **Settings/General** and to *Environment Variables* add:
    1. *MYSQL_ROOT_PASSWORD*
    2. *MYSQL_USER*
    3. *MYSQL_PASSWORD*
    4. *MYSQL_DATABASE*
5. After clicking **SAVE** button the image is automatically restarted

##MYSQL Database schema initialization
There are following run configurations available to initialize database schemas for services:

1. **Applicant clean database** - cleans applicant service database schema
2. **Applicant migrate database to latest** - runs all migration scripts and initializes data for applicant service
3. **Application clean database** - cleans application service database schema
4. **Application migrate database to latest** - runs all migration scripts and initializes data for applicant service

##Installing DynamoDB database in Kitematic
1. Click **+NEW**
2. Search for _dynamodb-local_ (amazon) and click **Create**
3. Go to **Settings/Ports** and modify published port to match docker port and save

##DynamoDB Database initialization
There are following run configurations available to initialize database for services:

1. **Job recreate database** - recreate job service related tables and initialized data for job service

##Config server
1. Git clone from config server repository to your local user folder (user.home env variable)  
git clone https://bitbucket.org/mgspd/demo-configs.git
2. In IDEA, go to **File -> Open** and select folder, where you cloned demo-configs repository
3. In the dialog that appears, select **New window**
4. In the newly opened project, open **readme.md** and follow the instructions
5. Before starting any other application, make sure config server is running - **Config Server App** run configuration

##Service discovery
For the purpose of microservices discovery, Eureka Server is used. Start it by running **Eureka Server App** run configuration.
Once started, you can open eureka UI - http://localhost:8761

##Installing messaging middleware in Kitematic - Apache Kafka 
1. Click **+NEW**
2. Search for Kafka-Zookeeper (johnnypark) and click **Create**
3. Go to **Settings/General** and to *Environment Variables* add:
    1. *ADVERTISED_HOST* - set the access IP of Kitematic (usually localhost) and save
4. Go to **Settings/Ports** and modify published ports to match docker ports and save

## Applications
1. **Applicant App** - start by running **Applicant App** run configuration
2. **Job App** - start by running **Job App** run configuration
3. **Application App** - start by running **Application App** run configuration
4. **Email App** - start by running **Email App** run configuration

##Maven profiles
1. **InstallDatabase** - disabled by default. Primarily used in database related run configurations.
2. **Repositories** - enabled by default. This picks nexus/maven repository configurations from maven settings, should be always enabled.
    
##Useful development URLs
* [BitBucket](https://bitbucket.org/mgspd/com.monster.demo/overview)